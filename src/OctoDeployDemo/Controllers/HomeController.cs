﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OctoDeployDemo.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            var setting = ConfigurationManager.AppSettings["RicksCustomAppSetting"];

            return View(setting as object);
        }

    }
}
